class CreateIssues < ActiveRecord::Migration[5.1]
  drop_table :issues
  def change
    create_table :issues do |t|
      t.string :title
      t.text :description
      t.integer :assigned_id
      t.string :tipo
      t.string :prioridad
      t.string :estado
      t.references :user, foreign_key: true

      t.timestamps
    end
    add_index :issues, [:user_id, :created_at]
  end
end
