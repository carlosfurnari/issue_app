json.extract! issue, :id, :title, :description, :user_id, :assigned_id, :tipo, :prioridad, :estado, :created_at, :updated_at
json.url issue_url(issue, format: :json)
